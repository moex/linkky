type rca_ontology ;;

operators FE;;

ontology_1
  o1_researcher
  o1_location
;;

ontology_2
  o2_inhabitant
  o2_place
;;

instances
  o1_researcher i1
  o1_researcher i2
  o1_researcher i3

  o2_inhabitant j1
  o2_inhabitant j2
  o2_inhabitant j3

  o1_location   i4
  o1_location   i5
  o1_location   i6

  o2_place      j4
  o2_place      j5
  o2_place      j6
;;

properties

  val i1  o1_fn   Thom
  val i2  o1_fn   Thomas
  val i3  o1_fn   Lisa
  val i1  o1_ln   Dupont
  val i2  o1_ln   Dubois
  val i3  o1_ln   Dubois
  obj i1  o1_hm   i4
  obj i2  o1_hm   i5
  obj i3  o1_hm   i6

  val i4 o1_ct Grenoble
  val i5 o1_ct Paris
  val i6 o1_ct Paris
  val i4 o1_st Henry
  val i5 o1_st Paul
  val i6 o1_st Jack
  obj i4 o1_ow i1
  obj i5 o1_ow i2
  obj i6 o1_ow i2

  val j1  o2_fn   Thomas
  val j2  o2_fn   Thomas
  val j3  o2_fn   Lisa
  val j1  o2_ln   Dupont
  val j2  o2_ln   Dubois
  val j3  o2_ln   Dubois
  obj j1  o2_hm   j4
  obj j2  o2_hm   j5
  obj j3  o2_hm   j5

  val j4 o2_ct Grenoble
  val j5 o2_ct Paris
  val j6 o2_ct Paris
  val j4 o2_st Henry
  val j5 o2_st Paul
  val j6 o2_st Jack
  obj j4 o2_ow j1
  obj j5 o2_ow j2
  obj j6 o2_ow j2


;;