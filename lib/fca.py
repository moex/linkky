#!/usr/bin/python3
# -*- coding: utf-8 -*-

from lib.logger import *
from lib.utilities import *
import itertools as it


class Concept:

    def __init__(self, context_id, extent, intent):
        self._context_id = context_id
        self._extent = frozenset(extent)
        self._intent = frozenset(intent)
        self._alt_intent = None
        self._red_intent = None
        self._red_extent = None
        self._dependencies = None
        self._names = set()
        self._fmeasure = None

    @property
    def context_id(self):
        return self._context_id

    @property
    def extent(self):
        return self._extent

    @property
    def intent(self):
        return self._intent

    @property
    def alt_intent(self):
        return self._alt_intent

    @alt_intent.setter
    def alt_intent(self, value):
        self._alt_intent = value

    @property
    def red_intent(self):
        return self._red_intent

    @red_intent.setter
    def red_intent(self, value):
        self._red_intent = value

    @property
    def red_extent(self):
        return self._red_extent

    @red_extent.setter
    def red_extent(self, value):
        self._red_extent = value

    @property
    def dependencies(self):
        return self._dependencies

    @dependencies.setter
    def dependencies(self, value):
        self._dependencies = value

    @property
    def names(self):
        return self._names

    @names.setter
    def names(self, values):
        self._names = values

    @property
    def fmeasure(self):
        return self._fmeasure

    @fmeasure.setter
    def fmeasure(self, value):
        self._fmeasure = value

    def __eq__(self, other):
        return isinstance(other, self.__class__) and\
            self.context_id == other.context_id and\
            self.intent == other.intent and\
            self.extent == other.extent

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self._context_id,
                     self._intent,
                     self._extent))

    def __str__(self):
        desc = 'context_id : %s\n' % self.context_id
        desc += 'intent     : %s\n' % self.intent
        desc += 'extent     : %s\n' % self.extent
        return desc


class Lattice:

    def __init__(self, context_id, concepts):
        self._context_id = context_id
        self._concepts = concepts
        self._parents = self.compute_covering(concepts)
        self._bottom = [c for c in self._concepts if not self.ideal(c)][0]
        self._top = [c for c in self._concepts if not self.filter(c)][0]

    @property
    def context_id(self):
        return self._context_id

    @property
    def top_concept(self):
        return self._top

    @property
    def bottom_concept(self):
        return self._bottom

    def filter(self, concept):
        return [c for c in self._concepts if concept.extent > c.extent]

    def ideal(self, concept):
        return [c for c in self._concepts if c.extent > concept.extent]

    def compute_covering(self, concepts):
        parents = dict([(c, set()) for c in concepts])
        for (i, j) in it.product(range(len(concepts)), repeat=2):
            iccpt, jccpt = concepts[i], concepts[j]
            if iccpt.extent < jccpt.extent:
                parents[jccpt].add(iccpt)
                for k in range(len(concepts)):
                    if iccpt.extent < concepts[k].extent < jccpt.extent:
                        parents[jccpt].remove(iccpt)
                        break
        return parents

    @property
    def concepts(self):
        return set(self._concepts)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and\
            self.concepts == other.concepts

    def __ne__(self, other):
        return not self.__eq__(other)

    def __len__(self):
        return len(self._concepts)

    def __getitem__(self, key):
        return self._concepts[key]

    def __contains__(self, value):
        return value in self._concepts

    def __str__(self):
        s = ""
        for c in self._concepts:
            s += "%s\n" % str(c)
        return s[:-1]

    def index(self, concept):
        return self._concepts.index(concept)

    def concept_for_name(self, name):
        for c in self:
            if c.names is not None and name in c.names:
                return c
        return None

    def parents(self, concept):
        return self._parents.get(concept, set())

    def children(self, concept):
        return set([c for c in self._concepts if concept in self.parents(c)])

    def clean(self, exporter=None):#lattice, 
        for c in self._concepts: #lattice:
            alt_intent = set(c.intent)
            for elmt in c.intent:
                if type(elmt) is not tuple:
                    continue
                if elmt[0] == '∀∃':
                    for op in ['∃', '∀']:
                        if (op,) + elmt[1:] in alt_intent:
                            alt_intent.remove((op,) + elmt[1:])
                c.alt_intent = frozenset(alt_intent)

        if exporter is not None:
            exporter.iteration += 1
            exporter.export_lattice(lattice)    

class FormalContext:

    def __init__(self, identifier, objects, attributes):
        self._identifier = identifier
        self._objects = objects
        self._attributes = attributes
        self._relations = DictSet()
        self._clean_lattice = False

    @property
    def identifier(self):
        return self._identifier

    @property
    def objects(self):
        return self._objects

    @property
    def attributes(self):
        return self._attributes

    @property
    def clean_lattice(self):
        return self._clean_lattice

    @clean_lattice.setter
    def clean_lattice(self, value):
        self._clean_lattice = value

    @property
    def is_empty(self):
        return len(self._relations) == 0

    @property
    def enum_relations(self):
        for (k, v_set) in self._relations.items():
            for v in v_set:
                yield (k, v)

    def check_relation(self, objt, attr):
        return attr in self._relations.get(objt)

    def add_relation(self, objt, attr):
        self._relations.add(objt, attr)

    def add_relations(self, relations):
        for (object, attribute) in relations:
            self.add_relation(object, attribute)

    def norris(self):
        concepts = [(set(), set(self.attributes))]
        for (i, object) in enumerate(self._objects):
            for c in concepts[:]:
                attr = self._relations.get(object)
                if c[1].issubset(attr):
                    c[0].add(object)
                    continue

                new, new_intent = True, c[1] & self._relations.get(object)
                for j in range(i):
                    j_objt = self._objects[j]
                    j_rela = self._relations.get(j_objt)
                    if new_intent.issubset(j_rela) and j_objt not in c[0]:
                        new = False
                        break
                if new:
                    concepts.append(({self._objects[i]} | c[0], new_intent))
        return [Concept(self.identifier, c[0], c[1]) for c in concepts]

    def build_lattice(self, exporter=None):
        Logger.log('Performing norris')
        Logger.log('Identifier: %s' % self._identifier)
        Logger.log('Number of objects: %d' % len(self._objects))
        Logger.log('Number of attributes: %d' % len(self._attributes))

        Logger.log('Calculating lattice')
        lattice = Lattice(self.identifier, self.norris())
        Logger.log('The lattice is composed of %d concepts' % len(lattice))
        if exporter is not None:
            exporter.export_fca_context(self)
            exporter.export_lattice(lattice)
        if self._clean_lattice:
            #self.clean(lattice, exporter)
            lattice.clean( exporter )
        return lattice
