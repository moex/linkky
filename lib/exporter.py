#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import glob
import re
import graphviz as gv
import itertools
from lib import *
from lib.utilities import *

class Exporter:

    def __init__(self, export_folder):
        last_char = export_folder[-1]
        self._export_folder = export_folder
        self._export_folder += '' if last_char == '/' else '/'
        self._iteration = 0
        self._shrink_extent = False
        self._shrink_intent = False
        self._flip_table = False
        self._flip_lattice = False
        self._substituations = []
        self.clean_export_folder()
        self._extended = False
        self._min_latex = False
        self._vtype = 'pdf'
        self._last_iteration = -1
        self._nbcols = len(self._colors)
        self.init_export_main()
        
    _colors = [
            'green', # used for the best (first is unused)
            'violet',
            'blue',
            'brown',
            'cyan',
            # 'lightgray', used for non compatible or empty keys
            'lime',
            'magenta',
            'olive',
            'orange',
            'pink',
            'purple',
            'red',
            'teal',
            #'white', used for non dependent keys
            'yellow',
        ]

    def getcolor(self, index):
        return self._colors[ index % self._nbcols ]

    @property
    def export_folder(self):
        return self._export_folder

    @property
    def tex_main(self):
        return self._tex_main

    @property
    def iteration(self):
        return self._iteration

    @iteration.setter
    def iteration(self, value):
        self._iteration = value

    @property
    def flip_table(self):
        return self.flip_table

    @flip_table.setter
    def flip_table(self, value):
        self._flip_table = value

    @property
    def flip_lattice(self):
        return self.flip_lattice

    @flip_lattice.setter
    def flip_lattice(self, value):
        self._flip_lattice = value

    @property
    def shrink_extent(self):
        return self._shrink_extent

    @shrink_extent.setter
    def shrink_extent(self, value):
        self._shrink_extent = value

    @property
    def shrink_intent(self):
        return self._shrink_intent

    @shrink_intent.setter
    def shrink_intent(self, value):
        self._shrink_intent = value

    @property
    def extended(self):
        return self._extended

    @extended.setter
    def extended(self, value):
        self._extended = value

    @property
    def min_latex(self):
        return self._min_latex

    @min_latex.setter
    def min_latex(self, value):
        self._min_latex = value

    @property
    def vtype(self):
        return self._vtype

    @vtype.setter
    def vtype(self, value):
        self._vtype = value

    @property
    def substituations(self):
        return self._substituations

    @substituations.setter
    def substituations(self, value):
        self._substituations = value

    def norm_filename(self, filename):
        return filename.replace('/', '_').replace('#', '_').replace(':', '_').replace('.', '_')

    def export_text(self, text, filename):
        text_file = open(self.export_folder + filename, "w", encoding='utf8')
        text_file.write(text)
        text_file.close()

    def clean_export_folder(self):
        if not os.path.exists(self._export_folder):
            os.makedirs(self._export_folder)
        else:
            for f in glob.glob(self._export_folder + '*'):
                os.remove(f)

    def export_fca_contexts(self, fcontexts):
        for fcontext in fcontexts.values():
            self.export_fca_context(fcontext)

    def export_rca_contexts(self, rcontexts, fcontexts):
        for rcontext in rcontexts.values():
            self.export_rca_context(rcontext, fcontexts)

    def export_lattices(self, lattices, index=None, vectors=None):
        for lattice in lattices.values():
            self.export_lattice(lattice, index, vectors)
        if vectors is None: # Include in main only the regular RCA rounds
            self.export_main_lattices(lattices)

    def export_fca_context(self, fcontext):
        fc_id = fcontext.identifier
        objects = fcontext.objects
        attributes = fcontext.attributes
        checker = fcontext.check_relation
        text = self.build_table(objects, attributes, checker)
        filename = 'fca_i%d_%s.tex' % (self._iteration, fc_id)
        filename = self.norm_filename( filename )
        self.export_text(text, filename)
        self.export_main_context(filename, fc_id)

    def export_rca_context(self, rcontext, fcontexts):
        rc_id = rcontext.identifier
        objts = fcontexts.get(rcontext.context_in).objects
        attrs = fcontexts.get(rcontext.context_out).objects
        checker = rcontext.check_relation
        text = self.build_table(objts, attrs, checker)
        filename = 'rca_i%d_%s.tex' % (self._iteration, rcontext.identifier)
        filename = self.norm_filename( filename )
        self.export_text(text, filename)
        self.export_main_context(filename, rc_id)

    def export_lattice(self, lattice, index=None, vectors=None):
        lattice.clean( None )
        self.reduce_concepts_labels(lattice)
        graph = self.export_graph(lattice, index, vectors)
        filename = 'lattice_i%d_%s' % (self._iteration, lattice.context_id)
        filename = self.norm_filename( filename )
        graph.render(self.export_folder + filename)

    def init_export_main( self ):
        self._tex_main = open(self.export_folder + "main.tex", "w")
        self._tex_main.write("\\documentclass[a4paper]{article}\n\n")
        self._tex_main.write("\\usepackage[margin=1cm,left=1cm,right=1cm]{geometry}\n")
        self._tex_main.write("\\usepackage[utf8]{inputenc}\n")
        self._tex_main.write("\\usepackage[x11names, rgb]{xcolor}\n\n")
        self._tex_main.write("\\usepackage{verbatim}\n\n")
        self._tex_main.write("\\usepackage{tikz}\n")
        self._tex_main.write("\\usetikzlibrary{decorations,arrows,shapes}\n")
        self._tex_main.write("\\usepackage{amsmath}\n\n")
        self._tex_main.write("\\usepackage{dot2texi}\n\n")
        self._tex_main.write("\\newcommand{\\uri}[2]{{\\small\\texttt{#1:#2}}}\n\n")
        self._tex_main.write("\\begin{document}\n\n")
        self._tex_main.write("\\tikzstyle{concept} = [\n")
        self._tex_main.write("        align=center,\n")
        self._tex_main.write("        draw,\n")
        self._tex_main.write("        rectangle split,\n")
        self._tex_main.write("        rectangle split parts=2,\n")
        self._tex_main.write("        rounded corners=.2cm,\n")
        self._tex_main.write("        minimum width=1.3cm,\n")
        self._tex_main.write("        minimum height=1.4cm,\n")
        self._tex_main.write("        text width=2.9cm\n")
        self._tex_main.write("      ]\n\n")
        self._tex_main.write("\\pagestyle{empty}\n\n")

    def close_export_main( self ):
        self._tex_main.write("\\end{document}\n\n")
        self._tex_main.close()

    def export_main_source(self, filename):
        self._tex_main.write("\\section{Experiment %s}\n\n" % filename[filename.rfind('/')+1:])
        self.print_import_source(filename)
            
    def print_import_source(self,filename):
        # Input text file
        if filename[0] == '/':
            self._tex_main.write("{\\footnotesize \\verbatiminput{%s}}\n\n" % (filename))
        elif filename[0] == '.':
            self._tex_main.write("{\\footnotesize \\verbatiminput{%s%s}}\n\n" % (os.getcwd(),filename[1:]))
        else:
            self._tex_main.write("{\\footnotesize \\verbatiminput{%s/%s}}\n\n" % (os.getcwd(),filename))
            
            
    def export_main_sources(self, ds1, ds2):
        self._tex_main.write("\\section{Experiment %s}\n\n" % ds1[ds1.rfind('/')+1:])
        # Input text file
        self._tex_main.write("\\subsection{Data source %s}\n\n" % ds1[ds1.rfind('/')+1:])
        self.print_import_source(ds1)
        #self._tex_main.write("{\\footnotesize \\verbatiminput{%s%s}}\n\n" % (os.getcwd(),ds1[1:])) 
        self._tex_main.write("\\subsection{Data source %s}\n\n" % ds2[ds2.rfind('/')+1:])
        self.print_import_source(ds2)
        #self._tex_main.write("{\\footnotesize \\verbatiminput{%s%s}}\n\n" % (os.getcwd(),ds2[1:]))

    def export_main_context(self, filename, id):
        if self._last_iteration != self._iteration:
            self._last_iteration = self._iteration
            self._tex_main.write("\\subsection{Iteration %d}\n\n" % self.iteration)
        # Draw contexts (local folder)
        idcomp = id.split('_')
        if len(idcomp) > 2: # FCA context
            init = idcomp[0]
            i = 1
        else:
            init = "R"
            i = 0
        frag1 = idcomp[i].split(':',1)
        frag2 = idcomp[i+1].split(':',1)
        self._tex_main.write( "$%s_{%s,%s}$:\n\n" % (init, frag1[1], frag2[1]) )
        #self._tex_main.write('\\begin{table}\n')
        self._tex_main.write( "{\centering\n" )
        self._tex_main.write( "{\\footnotesize\\input{%s}}}\n\n" % filename)
        #self._tex_main.write('  \\caption{Iteration %d : %s}\n'% (self._iteration, id))
        #self._tex_main.write('  \\label{table:%s}\n' % id)
        #self._tex_main.write('  \\end{table}\n')

    def export_main_lattices(self, lattices):
        # Draw graphs (ideally all graphs, like all tables
        self._tex_main.write("\\begin{center}\n")
        for lattice in lattices.values():
            filename = 'lattice_i%d_%s' % (self._iteration, lattice.context_id)
            filename = self.norm_filename( filename )
            self._tex_main.write("\\begin{tikzpicture}[scale=.3,font=\\scriptsize]\n\n")
            #self._tex_main.write("\\begin{scope}[scale=.3,xshift=5cm]\n")
            self._tex_main.write("\\begin{dot2tex}[dot,tikz,codeonly,options=-traw]\n")
            self._tex_main.write("\\input{%s}\n" % (filename))
            self._tex_main.write("\\end{dot2tex}\n")
            # Name the concepts
            #for concept in lattice: 
            #    if len(concept.names) > 0: # It exists
            #        ctid = self.concept_name(concept, 0)
            #        self._tex_main.write("\\node[anchor=south] at (%s.north east) {%s};\n" % (ctid, ctid))
            # JE: print measures (F-measure is only computed in the end, hence it is none!)
            for (index, concept) in enumerate(lattice):
                if len(concept.names) > 0: # It exists
                    ctid = self.concept_name(concept, 0)
                    self._tex_main.write("\\node[anchor=south] at (%s.north east) {%s};\n" % (ctid, ctid))
                else:
                    self._tex_main.write("\\node[anchor=south] at (%d.north east) {$k_{%d}$};\n" % (index,index))
            #    self._tex_main.write("--> %d + %s;" % (index,concept.fmeasure))
            #    ctid = self.concept_name(concept,index)
            #    if concept.fmeasure is not None:
            #        self._tex_main.write("\\node[anchor=west] at (%s.south east) {%s};\n" % (ctid, concept.fmeasure))
            #self._tex_main.write("\\end{scope}\n\n")
            self._tex_main.write("\\end{tikzpicture}\n")
        self._tex_main.write("\\end{center}\n\n")

    def build_table(self, objects, attributes, checker):
        if self._flip_table:
            objects, attributes = attributes, objects
            checker = lambda x, y: checker(y, x)
        if self._min_latex == True:
            shortattrs = [ a for a in attributes if a[0] != '∀∃' ]
        else:
            shortattrs = attributes
        latex = ''
        latex += '  \\begin{tabular}{|l|' + len(shortattrs) * 'c|' + '}\n'
        latex += '    \\hline\n & '
        latex += ' & '.join(['\\rotatebox{90}{'+self.latex_tuple(a)+'}' for a in shortattrs]) + '\n'
        latex += '     \\\\\\hline\n'
        for o in objects:
            latex += '        %s' % self.latex_tuple(o)
            for a in shortattrs:
                if checker(o, a):
                    latex += '& $\\times$ '
                else:
                    latex += '&          '
            latex += '\\\\\\hline\n'
        latex += '  \\end{tabular}\n'
        return latex

    def label_elements(self, elements, shrink):
        desc, short = '', False
        if len(elements) == 0:
            desc += '∅'
        elif not shrink and len(elements) <= 100:
            for i in range(0, len(elements), 2):
                desc += ', '.join([str(i) for i in elements[i:i+2]]) + '\n'
            desc = desc[:-1]
        else:
            desc = '%s elements' % (len(elements))
        return desc

    def label_concept(self, lattice, concept):
        if len(concept.names) > 0 and concept.fmeasure is not None:
            header = '%s - %.2f\n' % (str(concept.names), concept.fmeasure)
        elif len(concept.names) > 0:
            header = '%s\n' % str(concept.names)
        elif concept.fmeasure is not None:
            header = '%.2f\n' % concept.fmeasure
        else:
            header = ''

        extent = list(concept.extent)
        if concept.alt_intent is None:
            intent = list(concept.intent)
        else:
            intent = list(concept.alt_intent)

        extent_desc = self.label_elements(extent, self.shrink_extent)
        intent_desc = self.label_elements(intent, self.shrink_intent)
        label = '%sIntent:\n%s\nExtent:\n%s' %\
                (header, intent_desc, extent_desc)

        label = label.replace('\'', '')
        for sub in self.substituations:
            label = label.replace(sub[0], sub[1])
        return label[:16000]

    def tikz_uri(self, element):
        frag = element.split(':',1)
        if self._min_latex == True:
            # will only work if textnumber
            pattern = re.match( '([^0-9]+)([0-9]+)', frag[1] )
            if pattern:
                return '%s_{%s}' % pattern.group(1,2)
            else:
                return frag[1]
        else:
            return '\\uri{%s}{%s}' % ( frag[0], frag[1] )

    def latex_quant(self, term):
        if term == '∃':
            return '\\exists'
        elif term == '∀':
            return '\\forall'
        elif term == '∀∃':
            return '\\forall\\exists'

    def latex_tuple(self, elmt):
        if len(elmt) == 2:
            # JE would be better to not have : but _ before the context name
            if ':' not in elmt[0]: # either link or qualified key condition
                quant = self.latex_quant(elmt[0])
                col = elmt[1].rfind(':')
                key = elmt[1][col+1:]
                parts = elmt[1][:col].split('_', 2 )
                u1 = self.tikz_uri( parts[0] )
                u2 = self.tikz_uri( parts[1] )
                return '$%s\\langle %s, %s\\rangle_{%s}$' % (quant, u1, u2, key)
            else:
                return '$\\langle %s, %s\\rangle$' % ( self.tikz_uri(elmt[0]), self.tikz_uri(elmt[1]) )
        elif len(elmt) == 3: # unqualified key condition
            quant = self.latex_quant(elmt[0])
            u1 = self.tikz_uri( elmt[1] )
            u2 = self.tikz_uri( elmt[2] )
            return '$%s\\langle %s, %s\\rangle$' % (quant, u1, u2)

    # Note that Jérémy mixed the extent for the intent
    def is_dependent_concept(self, concept):
        for intent in list(concept.red_intent):
            if len(intent) == 2:
                return True
        return False

    def label_tikz_elements(self, elements, shrink):
        desc, short = '', False
        if len(elements) == 0:
            desc += '$\empty$'
        elif not shrink and len(elements) <= 100:
            first = True
            for elmt in set(elements):
                if type(elmt) is tuple:
                    if first == True:
                        first = False
                    else:
                        desc += ', '
                    desc += self.latex_tuple( elmt )            
        else:
            desc = '%s elements' % (len(elements))
        return desc

    def label_tikz_concept(self, lattice, concept):
        meas = None
        #if meas != None:
        # has to be printed aside
        if len(concept.names) > 0:
            header = '%s\n' % str(concept.names)

        extent = list(concept.red_extent)
        intent = list(concept.red_intent)

        extent_desc = self.label_tikz_elements(extent, self.shrink_extent)
        intent_desc = self.label_tikz_elements(intent, self.shrink_intent)
        label = '%s\n\\nodepart{two}\n%s' %\
                (intent_desc, extent_desc)

        label = label.replace('\'', '')
        for sub in self.substituations:
            label = label.replace(sub[0], sub[1])
        return label[:16000]

    def concept_name(self, concept, index):
        if len(concept.names) < 1:
            return str(index)
        else:
            lab = ''
            for name in concept.names:
                idx = name.rfind(':')+1
                if len(lab) > 0:
                    lab += '-'
                lab += str(name[idx:])
            return lab

    def build_tikz_lattice_graph(self, lattice, v_index, vectors):

        graph = gv.Graph(format='dot')
        graph.graph_attr.update({'nodesep':'2'})
    
        graph.node_attr.update({'style': 'concept'})

        for (index, concept) in enumerate(lattice):
            vector_index = None
            if vectors is not None:
                for (vi, vector) in enumerate(vectors):
                    for linkkey in vector[0]:
                        is_valid = len(linkkey.ccpt_names) != 0
                        is_valid &= concept.names == linkkey.ccpt_names
                        #is_valid &= (concept.names == linkkey.ccpt_names or concept.alt_intent == linkkey.keys)
                        #is_valid &= concept.extent == linkkey.keys
                        #is_valid &= concept.alt_intent == linkkey.keys
                        is_valid &= vector_index != v_index
                        if is_valid:
                            vector_index = vi

            ccpt_rep = self.label_tikz_concept(lattice, concept)
            fillcolor = None
            if vector_index is None: # Not in a vector
                if self.is_dependent_concept( concept ):
                    graph.node(self.concept_name(concept,index), ccpt_rep, style='concept,fill=lightgray!20',xlabel=self.concept_name(concept,index))
                else:
                    graph.node(self.concept_name(concept,index), ccpt_rep,xlabel=self.concept_name(concept,index))
            else:
                if vector_index == v_index: # The best
                    graph.node(self.concept_name(concept,index), ccpt_rep, style='concept,fill=green!50',xlabel=self.concept_name(concept,index))
                else:
                    fillcolor = 'concept,fill=%s!50' % self.getcolor(vector_index)
                    graph.node(self.concept_name(concept,index), ccpt_rep, style=fillcolor,xlabel=self.concept_name(concept,index))

            for pconcept in lattice.parents(concept):
                pindex = lattice.index(pconcept)
                if self._flip_lattice:
                    graph.edge(self.concept_name(pconcept,pindex), self.concept_name(concept,index))
                else:
                    graph.edge(self.concept_name(concept,index), self.concept_name(pconcept,pindex))
        return graph

    def build_lattice_graph(self, lattice, v_index, vectors):

        graph = gv.Graph(format='pdf')
        graph.node_attr.update({
            'fontname': 'Helvetica',
            'shape': 'box',
            'fontcolor': 'black',
            'color': 'black',
            'style': 'filled',
            'fillcolor': '#FFFFFF',
        })

        for (index, concept) in enumerate(lattice):
            vector_index = None
            if vectors is not None:
                for (vi, vector) in enumerate(vectors):
                    for linkkey in vector[0]:
                        is_valid = len(linkkey.ccpt_names) != 0
                        is_valid &= concept.names == linkkey.ccpt_names
                        is_valid &= vector_index != v_index
                        if is_valid:
                            vector_index = vi

            fillcolor = None
            if vector_index is not None:
                if vector_index == v_index:
                    fillcolor = 'green'
                else:
                    fillcolor = self.getcolor(vector_index) #self._colors[vector_index % len(colors)]

            ccpt_rep = self.label_concept(lattice, concept)
            if fillcolor is None:
                graph.node(str(index), ccpt_rep)
            else:
                graph.node(str(index), ccpt_rep,
                           fillcolor=fillcolor,
                           style='filled')

            for pconcept in lattice.parents(concept):
                pindex = lattice.index(pconcept)
                if self._flip_lattice:
                    graph.edge(str(pindex), str(index))
                else:
                    graph.edge(str(index), str(pindex))
        return graph


    def export_graph(self, lattice, v_index, vectors):
        self.reduce_concepts_labels(lattice)
        if self._vtype == 'tikz':
            return self.build_tikz_lattice_graph(lattice, v_index, vectors)
        else:
            return self.build_lattice_graph(lattice, v_index, vectors)

    # if qualified & same qualifier & same properties & concept subsumed
    def subsumed(self, intent1, intent2):
        if len(intent1) == 2 and len(intent2) == 2 and intent1[0] == intent2[0]:
            col1 = intent1[1].rfind(':')
            parts1 = intent1[1][:col1].split('_', 2 )
            col2 = intent2[1].rfind(':')
            parts2 = intent2[1][:col2].split('_', 2 )
            if parts1[0] == parts2[0] and parts1[1] == parts2[1]:
                key1 = intent1[1][col1+1:]
                key2 = intent2[1][col2+1:]
                # TO COMPARE AND RETURN THE MOST SPECIFIC!!
                # WE SHOULD BE ABLE TO FIND THE LATTICE AND IDENTIFY THE JOIN
                return key2
        return None
        if (intent1 == intent2): # why not test first?
            return intent1
        else:
            return None

    def latex_tuple3(self, elmt):
        if len(elmt) == 2:
            # JE would be better to not have : but _ before the context name
            if ':' not in elmt[0]: # either link or qualified key condition
                return '$%s\\langle %s, %s\\rangle_{%s}$' % (quant, u1, u2, key)
            else:
                return '$\\langle %s, %s\\rangle$' % ( self.tikz_uri(elmt[0]), self.tikz_uri(elmt[1]) )
        
    # Note that Jérémy mixed the extent for the intent
    def reduce_concepts_labels(self, lattice):
        # Could add simply lattice.clean( None )
        # This part is correct
        for concept in lattice:
            if concept.alt_intent:
                alt_intent = set(concept.alt_intent)
            else:
                alt_intent = set(concept.intent)
            for elmt in set(alt_intent):
                for pconcept in lattice.children(concept):
                    if pconcept.alt_intent:
                        pintent = set(pconcept.alt_intent)
                    else:
                        pintent = set(pconcept.intent)
                    if elmt in pintent:
                        alt_intent.discard(elmt)
            # now I must test that all concept in the remaining alt intent
            # is not subsumed by another one.
            alt2_intent = set(alt_intent)
            for x,y in itertools.combinations(alt2_intent, 2):
                # JE: this is a trick unfortunatelly we cannot fully test subsumed here...
                tosuppress = self.subsumed( x, y )
                if ( x == y ):
                    alt_intent.discard( y )
                elif ( tosuppress != None ):
                    alt_intent.discard( y )
                    alt_intent.discard( x )
                    alt_intent.add( (x[0], (x[1]+"\\wedge "+tosuppress)) )

            concept.red_intent = frozenset(alt_intent)
            alt_extent = set(concept.extent)
            for elmt in set(concept.extent):
                for pconcept in lattice.parents(concept):
                    if elmt in set(pconcept.extent):
                        alt_extent.discard(elmt)

            concept.red_extent = frozenset(alt_extent)
