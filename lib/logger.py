#coding=utf-8

class Logger(object):

    _enable = False

    @staticmethod
    def get_enable():
        return Logger._enable

    @staticmethod
    def set_enable(value):
        Logger._enable = value

    @staticmethod
    def log(*values):
        if Logger._enable:
            print(*values)
