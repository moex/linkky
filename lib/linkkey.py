#!/usr/bin/python3
# -*- coding: utf-8 -*-

class AbstractLinkkey:

    def __init__(self, keys):
        self._keys = keys
        self._fmeasure = 0.0
        self._coverage = 0.0
        self._discrimi = 0.0

    @property
    def keys(self):
        return self._keys

    @property
    def fmeasure(self):
        return self._fmeasure

    @fmeasure.setter
    def fmeasure(self, value):
        self._fmeasure = value

    @property
    def coverage(self):
        return self._coverage

    @coverage.setter
    def coverage(self, value):
        self._coverage = value

    @property
    def discrimi(self):
        return self._discrimi

    @discrimi.setter
    def discrimi(self, value):
        self._discrimi = value

    def __str__(self):
        result = 'names    : %s\n' % str(self._ccpt_names)
        result += 'concept  : %s\n' % str(self.keys)
        result += 'fmeasure : %.2f\n' % self.fmeasure
        result += 'discrimi : %.2f\n' % self.discrimi
        result += 'coverage : %.2f\n' % self.coverage
        return result


class DatabaseLinkkey(AbstractLinkkey):

    def __init__(self, keys):
        AbstractLinkkey.__init__(self, keys)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and\
            self.keys == other.keys

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(tuple(self.keys))


class OntologyLinkkey(AbstractLinkkey):

    def __init__(self, class_0, class_1, keys, ccpt_names):
        AbstractLinkkey.__init__(self, keys)
        self._class_0 = class_0
        self._class_1 = class_1
        self._ccpt_names = ccpt_names

    @property
    def class_0(self):
        return self._class_0

    @property
    def class_1(self):
        return self._class_1

    @property
    def classes(self):
        return {self._class_0, self._class_1}

    @property
    def ccpt_names(self):
        return self._ccpt_names

    def __eq__(self, other):
        return isinstance(other, self.__class__) and\
            self.keys == other.keys and\
            self.class_0 == other.class_0 and\
            self.class_1 == other.class_1

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((tuple(self.keys),
                     self.class_0,
                     self.class_1))

    def __str__(self):
        result = "class 0  : %s\n" % self.class_0
        result += "class 1  : %s\n" % self.class_1
        return result + super().__str__()
