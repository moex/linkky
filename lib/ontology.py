#!/usr/bin/python3
# -*- coding: utf-8 -*-

from lib.utilities import *


class OGYClass:

    def __init__(self, class_id):
        self._class_id = class_id
        self._val_property_ids = set()
        self._obj_property_ids = set()
        self._instance_ids = set()
        self._dependencies = set()

    @property
    def class_id(self):
        return self._class_id

    @property
    def val_property_ids(self):
        return self._val_property_ids

    @property
    def obj_property_ids(self):
        return self._obj_property_ids

    @property
    def property_ids(self):
        return self._val_property_ids |\
               self._obj_property_ids

    @property
    def instance_ids(self):
        return self._instance_ids

    @property
    def dependencies(self):
        return self._dependencies

    def add_val_property_id(self, prop_id):
        self._val_property_ids.add(prop_id)

    def add_obj_property_id(self, prop_id):
        self._obj_property_ids.add(prop_id)

    def add_instance_id(self, inst_id):
        self._instance_ids.add(inst_id)

    def add_dependency(self, class_id):
        self._dependencies.add(class_id)


class OGYInstance:

    def __init__(self, class_id, instance_id):
        self._class_id = class_id
        self._instance_id = instance_id
        self._val_properties = DictSet()
        self._obj_properties = DictSet()
        self._properties_dict = Dict()

    @property
    def class_id(self):
        return self._class_id

    @property
    def instance_id(self):
        return self._instance_id

    def add_val_property(self, prop_id, value):
        self._val_properties.add(prop_id, value)

    def add_obj_property(self, prop_id, value):
        self._obj_properties.add(prop_id, value)

    def add_class_property(self, prop_id, class_id):
        self._properties_dict.set(prop_id, class_id)

    def values_for_property(self, prop_id):
        return self._val_properties.get(prop_id)

    def objects_for_property(self, prop_id):
        return self._obj_properties.get(prop_id)

    def class_for_property(self, prop_id):
        return self._properties_dict.get(prop_id)

    def has_property(self, prop_id, value):
        return value in self._val_properties.get(prop_id) or\
               value in self._obj_properties.get(prop_id)


class Ontology:

    def __init__(self, classes0, classes1):
        self._classes0 = classes0
        self._classes1 = classes1
        self._classes_by_id = Dict()
        self._instances_by_id = Dict()
        self._instances_by_class_id = DictSet()
        self._linked_classes = DictSet()
        self._instance_ids_by_vals = Dict()
        self._same_as = set()
        self._nb_instances = 0
        self._nb_val_properties = 0
        self._nb_obj_properties = 0
        self._sameas_items = set()

    @property
    def classes0(self):
        return self._classes0

    @property
    def classes1(self):
        return self._classes1

    @property
    def nb_instances(self):
        return self._nb_instances

    @property
    def nb_val_properties(self):
        return self._nb_val_properties

    @property
    def nb_obj_properties(self):
        return self._nb_obj_properties

    @property
    def nb_properties(self):
        return self._nb_val_properties +\
               self._nb_obj_properties

    @property
    def linked_classes(self):
        return self._linked_classes

    def add_instance(self, class_id, inst_id):
        ogy_class = self._classes_by_id.get(class_id)
        if ogy_class is None:
            ogy_class = OGYClass(class_id)
            self._classes_by_id.set(class_id, ogy_class)

        ogy_class.add_instance_id(inst_id)
        self._instances_by_class_id.add(class_id, inst_id)
        inst = OGYInstance(class_id, inst_id)
        self._instances_by_id.set(inst_id, inst)
        self._nb_instances += 1

    def add_val_property(self, inst_id, prop_id, value):
        ogy_inst = self._instances_by_id.get(inst_id)
        if ogy_inst is None:
            print("instance is unknown %s", inst_id)
            return

        ogy_inst.add_val_property(prop_id, value)
        ogyclass = self._classes_by_id.get(ogy_inst.class_id)
        ogyclass.add_val_property_id(prop_id)
        self._nb_val_properties += 1

    def add_obj_property(self, inst_id, prop_id, value):
        fr_inst = self._instances_by_id.get(inst_id)
        to_inst = self._instances_by_id.get(value)
        if fr_inst is None or to_inst is None:
            if value not in self._sameas_items:
                print('One instance is unknown %s, %s' % (inst_id,  value))
                return

        fr_inst.add_obj_property(prop_id, value)
        fr_class = self._classes_by_id.get(fr_inst.class_id)
        fr_class.add_obj_property_id(prop_id)

        if to_inst is not None:
            fr_class.add_dependency(to_inst.class_id)
            fr_inst.add_class_property(prop_id, to_inst.class_id)
            linked_class_key = (fr_inst.class_id, to_inst.class_id)
            self._linked_classes.add(linked_class_key, prop_id)
        self._nb_obj_properties += 1

    # NOT COMPLETELY IMPLEMENTED
    def add_same_as(self, inst_id_0, inst_id_1):
        self._sameas_items.add(inst_id_0)
        self._sameas_items.add(inst_id_1)
        return self._same_as.add(frozenset({inst_id_0, inst_id_1}))

    def get_class(self, class_id):
        return self._classes_by_id.get(class_id)

    def get_instance(self, inst_id):
        return self._instances_by_id.get(inst_id)

    def get_dependencies(self, *class_ids):
        dependencies = set()
        for class_id in class_ids:
            ogyclass = self._classes_by_id.get(class_id)
            dependencies |= ogyclass.dependencies
        return dependencies

    def get_instances(self, class_id):
        return self._instances_by_class_id.get(class_id)

    def has_properties(self, o01, v01, prop_01):
        inst0 = self.get_instance(o01[0])
        inst1 = self.get_instance(o01[1])
        return inst0.has_property(prop_01[0], v01[0]) and \
               inst1.has_property(prop_01[1], v01[1])

    def has_same_as(self, inst_id_0, inst_id_1):
        return frozenset({inst_id_0, inst_id_1}) in self._same_as
