#!/usr/bin/python3
# -*- coding: utf-8 -*-

from copy import deepcopy
from lib.logger import *
from lib.utilities import *


class RelationalContext:

    def __init__(self, identifier, context_in, context_out):
        self._identifier = identifier
        self._context_in = context_in
        self._context_out = context_out
        self._relations = DictSet()

    @property
    def identifier(self):
        return self._identifier

    @property
    def context_in(self):
        return self._context_in

    @property
    def context_out(self):
        return self._context_out

    @property
    def is_empty(self):
        return len(self._relations) == 0

    def count_relations(self, obj):
        return len(self._relations.get(obj))

    def check_relation(self, obj, attr):
        return attr in self._relations.get(obj)

    def add_relation(self, obj, attr):
        self._relations.add(obj, attr)

    def add_relations(self, relations):
        for (obj, attr) in relations:
            self.add_relation(obj, attr)


class RCAProcess:
    def __init__(self, fcontexts, rcontexts, operators):
        self._fcontexts = fcontexts
        self._rcontexts = rcontexts
        self._tcontexts = deepcopy(fcontexts)
        self._lattices_old = Dict()
        self._lattices_new = Dict()
        self._iteration = 0
        self._max_iterations = 20
        self._concept_ids = DDict()
        self._operators = operators
        self._clean_lattice = True

    @property
    def iteration(self):
        return self._iteration

    @property
    def operators(self):
        return self._operators

    @operators.setter
    def operators(self, value):
        self._operators = value

    @property
    def clean_lattice(self):
        return self._clean_lattice

    @clean_lattice.setter
    def clean_lattice(self, value):
        self._clean_lattice = value

    @property
    def fcontexts(self):
        return self._fcontexts

    @property
    def rcontexts(self):
        return self._rcontexts

    @property
    def lattices(self):
        return self._lattices_old

    @property
    def max_iterations(self):
        return self._max_iterations

    @max_iterations.setter
    def max_iterations(self, value):
        self._max_iterations = value

    def update_concepts(self):
        self._lattices_old = deepcopy(self._lattices_new)
        for identifier, fcontext in self._tcontexts.items():
            lattice = fcontext.build_lattice()
            self._lattices_new.set(identifier, lattice)

    def check_fixpoint(self):
        return self._lattices_old == self._lattices_new or \
               self._iteration > self._max_iterations - 1

    def check_operator(self, operator, matches_count, max_count):
        if operator == '∃':
            return matches_count > 0
        elif operator == '∀':
            return matches_count == max_count
        elif operator == '∀∃':
            return matches_count == max_count and max_count > 0
        else:
            return False

    def extend_step(self, fcontext, rcontext, concept, operator):
        extent_val = tuple(concept.extent)
        ccpt_id = "%s:C%d" % (rcontext.identifier, len(self._concept_ids))
        ccpt_intent = (rcontext.identifier, rcontext.context_out, extent_val)
        ccpt_name = self._concept_ids.get_value(ccpt_intent, ccpt_id)
        attr_name, nb_add = (operator, ccpt_name), 0

        for fc_val in fcontext.objects:
            matches_count = 0
            for obj_val in concept.extent:
                if rcontext.check_relation(fc_val, obj_val):
                    matches_count += 1

            max_count = rcontext.count_relations(fc_val)
            if self.check_operator(operator, matches_count, max_count):
                nb_add += 1
                fcontext.add_relation(fc_val, attr_name)

        if nb_add > 0:
            self._concept_ids.set(ccpt_intent, ccpt_name)
            concept.names.add(ccpt_name)
            fcontext.attributes.append(attr_name)

    def extend_fcontext(self, fc_identifier):
        fcontext = deepcopy(self._fcontexts[fc_identifier])
        rcontexts = [rc for rc in self._rcontexts.values()
                     if rc.context_in == fc_identifier]

        for rcontext in rcontexts:
            context_out = rcontext.context_out
            for concept in self._lattices_new.get(context_out):
                for operator in self.operators:
                    self.extend_step(fcontext, rcontext, concept, operator)
        self._tcontexts[fc_identifier] = fcontext

    def get_name_set(self, concept):
        ids, intent = set(), tuple(concept.intent)
        for key, value in self._concept_ids.kv_items():
            if key[1] == concept.context_id and key[2] == intent:
                ids.add(value)
        return ids

    def get_name(self, concept):
        ids = self.get_name_set(concept)
        ids = set([str(id) for id in ids])
        return 'C#' if len(ids) == 0 else '-'.join(ids)

    def get_concept(self, name):
        return self._concept_ids.get_key(name)

    def print_concepts(self):
        print('%s\nnumber of iterations : %d / %d\n%s'
              % (78 * '#', self._iteration, self.max_iterations, 78 * '#'))
        for (fc_identifier, lattice) in self._lattices_new.items():
            print(78 * '=' + '\n' + fc_identifier + '\n' + 78 * '=')
            for concept in lattice:
                ccpt_name = self.get_name(concept)
                ccpt_name += (6 - len(ccpt_name)) * ' '
                intent = [str(v) for v in concept.intent]
                extent = [str(v) for v in concept.extent]
                intent.sort(), extent.sort()
                print(78 * '-' + '\n' + ccpt_name)
                print('\tintent :', intent)
                print('\textent :', extent)

    def clean_lattices(self, exporter=None):
        for identifier, lattice in self._lattices_old.items():
            for c in lattice:
                alt_intent, rccpt_ids, ccpt_dep = set(c.intent), set(), set()
                for elmt in c.intent:
                    if type(elmt) is tuple and len(elmt) == 2:
                        rccpt_ids.add(elmt[1])

                for rccpt_id in rccpt_ids:
                    fc_id = self._concept_ids.get_key(rccpt_id)[1]
                    rlattice = self._lattices_old.get(fc_id)
                    rccpt = rlattice.concept_for_name(rccpt_id)
                    for pccpt in rlattice.children(rccpt):
                        for op in ['∃', '∀', '∀∃']:
                            for name in pccpt.names:
                                if (op, name) in alt_intent:
                                    alt_intent.remove((op, name))

                for elmt in set(alt_intent):
                    if type(elmt) is not tuple:
                        continue
                    if elmt[0] == '∀∃':
                        for op in ['∃', '∀']:
                            if (op,) + elmt[1:] in alt_intent:
                                alt_intent.remove((op,) + elmt[1:])
                    if len(elmt) == 2:
                        ccpt_dep.add(elmt[1])

                c.dependencies = ccpt_dep
                c.alt_intent = frozenset(alt_intent)

        if exporter is not None:
            exporter.iteration += 1
            exporter._tex_main.write( '\\subsection{Iteration %s (convergence)}' % exporter.iteration )
            exporter.export_lattices(self.lattices)

    def build_lattices(self, exporter=None):
        self._operators = guard_operators(self._operators)
        Logger.log('Performing RCA process')
        Logger.log('Operators %s' % ' '.join(self.operators).encode('utf-8'))
        Logger.log(78 * '-' + '\nIteration: 0/%d' % self.max_iterations)
        if exporter is not None:
            exporter.export_fca_contexts(self._fcontexts)
            exporter.export_rca_contexts(self._rcontexts, self._fcontexts)

        self.update_concepts()
        while not self.check_fixpoint():
            self._iteration += 1
            iter = (self.iteration, self.max_iterations)
            Logger.log(78 * '-' + '\nIteration: %d/%d' % iter)
            for identifier in self._fcontexts.keys():
                self.extend_fcontext(identifier)

            self.update_concepts()
            if exporter is not None:
                exporter.iteration += 1
                exporter.export_fca_contexts(self._tcontexts)
                exporter.export_lattices(self._lattices_old)

        if self.clean_lattice:
            self.clean_lattices(exporter)
        return self.lattices
