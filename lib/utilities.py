#! /usr/bin/Python3.6


def guard_operators(operators):
    if len(operators) == 0:
        return '∃', '∀', '∀∃'
    new_operators = set()
    for op in operators:
        new_operators.add({'E': '∃', 'F': '∀', 'FE': '∀∃'}.get(op, op))
    return tuple(new_operators)


def list_apply(list, func, ungroup=False):
    for element in list:
        func(*element if ungroup else element)


def list_reshape(list, length):
    result = []
    for i in range(0, len(list), length):
        result.append(tuple(list[i:i + length]))
    return result


class Dict:

    def __init__(self):
        self.kv_dict = {}

    def get(self, key, default=None):
        return self.kv_dict.get(key, default)

    def set(self, key, value):
        self.kv_dict[key] = value

    def pop(self, key, default=None):
        return self.kv_dict.pop(key, default)

    def keys(self):
        return self.kv_dict.keys()

    def values(self):
        return self.kv_dict.values()

    def items(self):
        return self.kv_dict.items()

    def clear(self):
        self.kv_dict.clear()

    def __len__(self):
        return len(self.kv_dict)

    def __repr__(self):
        return self.kv_dict.__repr__()

    def __str__(self):
        return self.kv_dict.__str__()

    def __eq__(self, other):
        return self.kv_dict.__eq__(other.kv_dict)

    def __ne__(self, other):
        return self.kv_dict.__ne__(other.kv_dict)

    def __hash__(self):
        return self.kv_dict.__hash__()


class DDict:

    def __init__(self):
        self.kv_dict = Dict()
        self.vk_dict = Dict()

    def get_value(self, key, default=None):
        return self.kv_dict.get(key, default)

    def get_key(self, value, default=None):
        return self.vk_dict.get(value, default)

    def pop_value(self, key, default=None):
        value = self.kv_dict.pop(key, default)
        self.vk_dict.pop(value)
        return value

    def pop_key(self, value, default=None):
        key = self.vk_dict.pop(value, default)
        self.kv_dict.pop(key)
        return key

    def set(self, key, value):
        self.kv_dict.set(key, value)
        self.vk_dict.set(value, key)

    def keys(self):
        return self.kv_dict.keys()

    def values(self):
        return self.kv_dict.values()

    def kv_items(self):
        return self.kv_dict.items()

    def vk_items(self):
        return self.vk_dict.items()

    def __len__(self):
        return len(self.kv_dict)

    def __repr__(self):
        return self.kv_dict.__repr__()

    def __str__(self):
        return self.kv_dict.__str__()

    def __eq__(self, other):
        return self.kv_dict.__eq__(other.kv_dict)

    def __ne__(self, other):
        return self.kv_dict.__ne__(other.kv_dict)

    def __hash__(self):
        return self.kv_dict.__hash__()


class DictSet:

    def __init__(self):
        self.kv_dict = Dict()

    def get(self, key, default=set()):
        return self.kv_dict.get(key, default)

    def add(self, key, value):
        set_val = self.kv_dict.get(key, set())
        set_val.add(value)
        self.kv_dict.set(key, set_val)

    def remove(self, key, value):
        set_val = self.kv_dict.get(key, set())
        set_val.remove(value)
        if len(set_val) == 0:
            self.kv_dict.pop(key)
        else:
            self.kv_dict.set(key, set_val)

    def pop(self, key, default=set()):
        return self.kv_dict.pop(key, default)

    def keys(self):
        return self.kv_dict.keys()

    def values(self):
        return self.kv_dict.values()

    def items(self):
        return self.kv_dict.items()

    def __len__(self):
        return len(self.kv_dict)

    def __repr__(self):
        return self.kv_dict.__repr__()

    def __str__(self):
        return self.kv_dict.__str__()

    def __eq__(self, other):
        return self.kv_dict.__eq__(other.kv_dict)

    def __ne__(self, other):
        return self.kv_dict.__ne__(other.kv_dict)

    def __hash__(self):
        return self.kv_dict.__hash__()


class DDictSet:

    def __init__(self):
        self.vk_dict = Dict()
        self.kv_dict = DictSet()

    def get_key(self, value, default=None):
        return self.kv_dict.get(value, default)

    def get_values(self, key, default=None):
        return self.vk_dict.get(key, default)

    def add(self, key, value):
        self.kv_dict.add(key, value)
        self.vk_dict.set(value, key)

    def remove(self, key, value):
        self.kv_dict.pop(key, value)
        self.vk_dict.pop(value, key)

    def pop(self, key, default=set()):
        return self.kv_dict.pop(key, default)

    def keys(self):
        return self.kv_dict.keys()

    def values(self):
        return self.kv_dict.values()

    def items(self):
        return self.kv_dict.items()

    def __len__(self):
        return len(self.kv_dict)

    def __repr__(self):
        return self.kv_dict.__repr__()

    def __str__(self):
        return self.kv_dict.__str__()

    def __eq__(self, other):
        return self.kv_dict.__eq__(other.kv_dict)

    def __ne__(self, other):
        return self.kv_dict.__ne__(other.kv_dict)

    def __hash__(self):
        return self.kv_dict.__hash__()
