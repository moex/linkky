#! /usr/bin/Python3.6


class Database:

    def __init__(self, attributes0, attributes1):
        self._attributes0 = attributes0
        self._attributes1 = attributes1
        self._tuples0 = []
        self._tuples1 = []

    @property
    def attributes0(self):
        return self._attributes0

    @property
    def attributes1(self):
        return self._attributes1

    @property
    def tuples0(self):
        return self._tuples0

    @property
    def tuples1(self):
        return self._tuples1

    def add_tuple0(self, tuple):
        if len(tuple) != len(self.attributes0):
            print("tuple doesn't the expected length")
            return
        self.tuples0.append(tuple)

    def add_tuples0(self, tuples):
        for tuple in tuples:
            self.add_tuple0(tuple)

    def add_tuple1(self, tuple):
        if len(tuple) != len(self.attributes1):
            print("tuple doesn't the expected length")
            return
        self.tuples1.append(tuple)

    def add_tuples1(self, tuples):
        for tuple in tuples:
            self.add_tuple1(tuple)