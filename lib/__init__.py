# -*- coding: utf-8 -*-
"""Linkky library"""

from lib.linkkey import *
from lib.fca import *
from lib.rca import *
from lib.database import *
from lib.ontology import *
from lib.dblinker import *
from lib.ogylinker import *
from lib.exporter import *
from lib.logger import *
