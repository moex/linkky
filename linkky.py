#!/usr/bin/python3
# -*- coding: utf-8 -*-

# JE: for whatever reason,
# $ python3 -c "import sys; print(sys.stdout.encoding)" | cat
# ANSI_X3.4-1968
# (even with LANG=en_US.UTF-8
# So running this code will output a:
# UnicodeEncodeError: 'ascii' codec can't encode characters in position 74-75: ordinal not in range(128)
# However, running:
# PYTHONIOENCODING=UTF-8 python3 linkky.py -t ./txt_data/dam-fca.txt -e ./export
# or LC_ALL=en_US.UTF-8
# Will be fine, so update your environment...

import re
import sys
import argparse
import random
import time
from rdflib import URIRef
from rdflib.graph import Graph
from rdflib.namespace import RDF
from lib import *


def parse_properties(terms):
    pattern = '\s*(%s+)\s*' % '[\w\_\-\:]'
    term_key = terms[0].lower()
    term_val = re.compile(pattern).findall(terms[1])
    return term_key, term_val


def parse_dictionary(terms):
    term_key = terms[0][:-1].lower()
    dict_val = {}
    for part in terms[1].split(';'):
        pattern = '^\s*(\w*)\s*(.*)\s*$'
        dict_terms = re.compile(pattern).findall(part)[0]
        entry_key, entry_val = parse_properties(dict_terms)
        dict_val[entry_key] = entry_val
    return term_key, dict_val


def parse_input_file(filename):
    values = {}
    with open(filename, 'r') as file:
        text = file.read().replace('\n', ' ')
        for part in text.split(';;'):
            if len(part.strip()) == 0:
                continue

            pattern = '^\s*([\w|>]*)\s*(.*)\s*$'
            terms = re.compile(pattern).findall(part)[0]
            if terms[0][-1] != '>':
                key, val = parse_properties(terms)
                values[key] = val
            elif terms[0][:-1] not in values:
                key, val = parse_dictionary(terms)
                values[key] = [val]
            else:
                key, val = parse_dictionary(terms)
                values[key].append(val)
    return values


def ungroup_relations(objects, attributes, relations):
    new_relations, curr_object = [], None
    for element in relations:
        if element in objects:
            curr_object = element
        elif element in attributes:
            new_relations.append((curr_object, element))
    return new_relations


def run_txt_fca_concepts(args, values, exporter):
    objects = values['objects']
    attributes = values['attributes']
    fcontext = FormalContext('FCA', objects, attributes)

    relations = ungroup_relations(objects, attributes, values['relations'])
    fcontext.add_relations(relations)

    start_time = time.time()
    lattice = fcontext.build_lattice(exporter)
    elapsed_time = time.time() - start_time  
    print('Time: %.3f s' % elapsed_time)
    for concept in lattice:
        print(78 * '-' + '\n' + str(concept))


def run_txt_fca_database(args, values, exporter):
    attributes_0 = values["attributes_0"]
    attributes_1 = values["attributes_1"]
    db = Database(attributes_0, attributes_1)

    tuples_0 = list_reshape(values['tuples_0'], len(attributes_0))
    tuples_1 = list_reshape(values['tuples_1'], len(attributes_1))
    db.add_tuples0(tuples_0)
    db.add_tuples1(tuples_1)

    linker = FCADatabaseLinker(db)
    start_time = time.time()
    linkkeys, best_linkey = linker.extract_linkkeys(exporter)
    elapsed_time = time.time() - start_time
    print('Time: %.3f s' % elapsed_time)

    print(78 * '#' + '\nThe candidate link keys are:')
    for linkkey in linkkeys:
        print(78 * '-' + '\n' + str(linkkey))
    print(78 * '#' + '\nThe best link key is:')
    print(best_linkey)


def run_txt_fca_ontology(args, values, exporter):
    alignments = list_reshape(values['alignments'], 2)
    classes_0 = set([v[0] for v in alignments])
    classes_1 = set([v[1] for v in alignments])
    ontology = Ontology(classes_0, classes_1)

    for instance in list_reshape(values['instances'], 2):
        ontology.add_instance(*instance)
    for property in list_reshape(values['properties'], 4):
        if property[0] == 'val':
            ontology.add_val_property(*property[1:])
        elif property[0] == 'obj':
            ontology.add_obj_property(*property[1:])

    linker = FCAOntologyLinker(ontology, alignments)
    for linkkey_dict in values.get('linkkeys', []):
        class_0 = linkkey_dict['class0'][0]
        class_1 = linkkey_dict['class1'][0]
        keys = list_reshape(linkkey_dict['keys'], 3)
        linkkey = OntologyLinkkey(class_0, class_1, keys, set())
        linker.add_linkkey(linkkey)

    start_time = time.time()
    linkkeys = linker.extract_linkkeys(exporter)
    elapsed_time = time.time() - start_time
    print('Time: %.3f s' % elapsed_time)

    for linkkey in linkkeys.values():
        print(78 * '-' + '\n' + str(linkkey))


def run_txt_rca_concepts(args, values, exporter):
    fcontexts = {}
    for fcontext_dict in values['formal_context']:
        fc_id = fcontext_dict['identifier'][0]
        fc_objects = fcontext_dict['objects']
        fc_attributes = fcontext_dict['attributes']
        fcontext = FormalContext(fc_id, fc_objects, fc_attributes)
        relations = fcontext_dict['relations']
        relations = ungroup_relations(fc_objects, fc_attributes, relations)
        fcontext.add_relations(relations)
        fcontexts[fcontext.identifier] = fcontext

    rcontexts = {}
    for rcontext_dict in values['relational_context']:
        fc_id = rcontext_dict['identifier'][0]
        context_in = rcontext_dict['context_in']
        context_out = rcontext_dict['context_out']
        rcontext = RelationalContext(fc_id, context_in[0], context_out[0])
        rc_objects = fcontexts[rcontext.context_in].objects
        rc_attributes = fcontexts[rcontext.context_out].objects
        relations = rcontext_dict['relations']
        relations = ungroup_relations(rc_objects, rc_attributes, relations)
        rcontext.add_relations(relations)
        rcontexts[rcontext.identifier] = rcontext

    operators = values['operators']
    process = RCAProcess(fcontexts, rcontexts, operators)
    process.max_iterations = 30
    start_time = time.time()
    process.build_lattices(exporter)
    elapsed_time = time.time() - start_time
    print('Time: %.3f s' % elapsed_time)
    process.print_concepts()


def run_txt_rca_ontology(args, values, exporter):
    classes_0 = set(values['ontology_1'])
    classes_1 = set(values['ontology_2'])
    ontology = Ontology(classes_0, classes_1)

    for inst in list_reshape(values['instances'], 2):
        ontology.add_instance(*inst)
    for prop in list_reshape(values['properties'], 4):
        if prop[0] == 'val':
            ontology.add_val_property(*prop[1:])
        else:
            ontology.add_obj_property(*prop[1:])

    linker = RCAOntologyLinker(ontology)
    linker.quantifiers = values['operators']
    extract_and_print_linkkeys( linker, exporter )

def extract_and_print_linkkeys( linker, exporter ):
    start_time = time.time()
    max_i, vector_linkkeys = linker.extract_linkkeys(exporter)
    elapsed_time = time.time() - start_time
    print('Time: %.3f s' % elapsed_time)
    print('Best: %d' % max_i)
    for i, vector in enumerate(vector_linkkeys):
        print(78 * '-' + '\nVector: %d score: %.2f' % (i, vector[1]))
        for linkkey in vector[0]:
            print(linkkey)
    
# JE: could be factorised (but not easy because ontology is a pair of ontology)
def run_ttl_rca_ontology(args, path_ttl0, path_ttl1, exporter):
    Logger.log('Reading turtles')
    g0, g1 = Graph(), Graph()
    g0.parse(path_ttl0, format="turtle")
    g1.parse(path_ttl1, format="turtle")
    sameas_uri = URIRef(u'http://www.w3.org/2002/07/owl#sameAs')
    type_uri = URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#type')

    Logger.log('Loading classes')
    classes_0, classes_1 = set(), set()
    for s, p, o in g0.triples((None, RDF.type, None)):
        classes_0.add(g0.namespace_manager.normalizeUri(o))
        #classes_0.add(o)
    for s, p, o in g1.triples((None, RDF.type, None)):
        classes_1.add(g1.namespace_manager.normalizeUri(o))
        #classes_1.add(o)
    ontology = Ontology(classes_0, classes_1)

    Logger.log('Loading instances')
    for s, p, o in g0.triples((None, RDF.type, None)):
        ontology.add_instance(g0.namespace_manager.normalizeUri(o), g0.namespace_manager.normalizeUri(s))
        #ontology.add_instance(o, s)
    for s, p, o in g1.triples((None, RDF.type, None)):
        ontology.add_instance(g1.namespace_manager.normalizeUri(o), g1.namespace_manager.normalizeUri(s))
        #ontology.add_instance(o, s)

    inst_ids = set()
    for c_id in classes_0:
        inst_ids |= ontology.get_instances(c_id)

    for (s, p, o) in g0:
        if g0.namespace_manager.normalizeUri(s) not in inst_ids or p == type_uri:
            continue
        if p == sameas_uri:
            ontology.add_same_as(g0.namespace_manager.normalizeUri(s), g0.namespace_manager.normalizeUri(o))
            continue
        if g0.namespace_manager.normalizeUri(o) in inst_ids:
            ontology.add_obj_property(g0.namespace_manager.normalizeUri(s), g0.namespace_manager.normalizeUri(p), g0.namespace_manager.normalizeUri(o))
        elif type(o) is not URIRef:
            ontology.add_val_property(g0.namespace_manager.normalizeUri(s), g0.namespace_manager.normalizeUri(p), o)

    inst_ids = set()
    for c_id in classes_1:
        inst_ids |= ontology.get_instances(c_id)

    for (s, p, o) in g1:
        if g1.namespace_manager.normalizeUri(s) not in inst_ids or p == type_uri:
            continue
        if p == sameas_uri:
            ontology.add_same_as(g1.namespace_manager.normalizeUri(s), g1.namespace_manager.normalizeUri(o))
            continue
        if g1.namespace_manager.normalizeUri(o) in inst_ids:
            ontology.add_obj_property(g1.namespace_manager.normalizeUri(s), g1.namespace_manager.normalizeUri(p), g1.namespace_manager.normalizeUri(o))
        elif type(o) is not URIRef:
            ontology.add_val_property(g1.namespace_manager.normalizeUri(s), g1.namespace_manager.normalizeUri(p), o)

    print('Performing interlinking')
    linker = RCAOntologyLinker(ontology)
    # Default values?
    linker.quantifiers = ['E', 'F', 'FE']
    extract_and_print_linkkeys( linker, exporter )

def run():
    if not sys.version_info >= (3, 6):
        print("linkky.py requires python 3.6 or newer; it even may work with older")
        # JE: neutralised
        #exit(0)

    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--txt', type=str, default=None,
                        help='set the filename of the input file txt or ttl')
    parser.add_argument('-t0', '--ttl0', type=str, default=None,
                        help='set the filename of the ttl input 0')
    parser.add_argument('-t1', '--ttl1', type=str, default=None,
                        help='set the filename of the ttl input 1')
    parser.add_argument('-e', '--export', type=str, default=None,
                        help='set the export folder')
    parser.add_argument('-v', '--verbose', action="store_true",
                        help='set the verbose mode')
    parser.add_argument('-ft', '--flip_table', action="store_true",
                        help='set to flip the exported table')
    parser.add_argument('-fl', '--flip_lattice', action="store_true",
                        help='set to flip the exported lattice')
    parser.add_argument('-si', '--shrink_intent', action="store_true",
                        help='shrink the intent in the exported lattice')
    parser.add_argument('-se', '--shrink_extent', action="store_true",
                        help='shrink the extent in the exported lattice')
    parser.add_argument('-f', '--graph_format', type=str, default='pdf',
                        help='set the format of the exported lattice (pdf, dot, tikz)')
    parser.add_argument('-m', '--min_latex', action="store_true",
                        help='print uri fragments as math ids (in LaTeX)')
    # JE: NIY
    parser.add_argument('-x', '--extended_display', action="store_true",
                        help='display full intent/extent in the exported lattice')
    parser.add_argument('-z', '--display_measures', action="store_true",
                        help='display measures in the exported lattice')
    args = parser.parse_args()


    txt_valid = args.txt is not None
    ttl_valid = args.ttl0 is not None and args.ttl1 is not None
    if not txt_valid and not ttl_valid:
        print('Invalid arguments')
        parser.print_help()
        exit(0)

    exporter = None
    if args.export is not None:
        exporter = Exporter(args.export)
        exporter.flip_table = args.flip_table
        exporter.flip_lattice = args.flip_lattice
        exporter.shrink_intent = args.shrink_intent
        exporter.shrink_extent = args.shrink_extent
        exporter.extended = args.extended_display
        exporter.vtype = args.graph_format
        exporter.min_latex = args.min_latex
    Logger.set_enable(args.verbose)
    if txt_valid:
        exporter.export_main_source(args.txt)
    else:
        exporter.export_main_sources(args.ttl0,args.ttl1)

    if txt_valid:
        values = parse_input_file(args.txt)
        rtype = values.get('type', [None])[0]
        if rtype == 'fca_concepts':
            run_txt_fca_concepts(args, values, exporter)
        elif rtype == 'rca_concepts':
            run_txt_rca_concepts(args, values, exporter)
        elif rtype == 'fca_database':
            run_txt_fca_database(args, values, exporter)
        elif rtype == 'fca_ontology':
            run_txt_fca_ontology(args, values, exporter)
        elif rtype == 'rca_ontology':
            run_txt_rca_ontology(args, values, exporter)
        else:
            print('Unknown input type')
    elif ttl_valid:
        path_ttl0, path_ttl1 = args.ttl0, args.ttl1
        run_ttl_rca_ontology(args, path_ttl0, path_ttl1, exporter)

    if args.export is not None:
        exporter.close_export_main()

if __name__ == '__main__':
    run()
