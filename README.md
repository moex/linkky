# Linkky

Linkky is a proof-of-concept of link key extraction based on formal and relational concept analysis.

Linkky is not meant to be a robust and efficient implementation of the proposed ideas and it not planned to be maintained.
It is only used for the purpose of performing research.

## Introduction

Linkky is a python library to perform data interlinking. It allows to extract link keys thanks to Formal context analysis (FCA) and Relational context analysis (RCA). 

The library uses Python 3 (developed with 3.6, known to work in 3.5) and was only tested on macOS 10.12, Ubuntu 16.04 and 18.04. 

It takes as input two data sources and can extract the set of global link key candidates.
It uses the Norris algorithm for performing FCA extended to deal with pairs of objects in the extent and quantified and qualified pairs of properties in the intent.
The RCA processing has been implemented by iteratively applying the scaling operators to the formal contexts.
RDF data can be loaded in the system through the RDF Library.
The best global link key may be extracted through an extension of the unsupervised link key selection measures to families of compatible link key candidates.

## How to use Linkky

### Installation

The framework uses two libraries GraphViz (for displaying lattices) and RDFLib (for parsing RDF). The simplest way to install them for python is to use `pip` (actually `pip3` for `python3`).

```
$ sudo apt install python3-pip
$ pip3 install graphviz
$ pip3 install rdflib
```
Graphviz also has to be installed on the execution platform.

### Parameters

The full set of options is as:
```
$ python3 lynkky.py --help
linkky.py requires python 3.6 or newer; it even may work with older
usage: linkky.py [-h] [-t TXT] [-t0 TTL0] [-t1 TTL1] [-e EXPORT] [-v] [-ft]
                 [-fl] [-si] [-se] [-f GRAPH_FORMAT] [-m] [-x]

optional arguments:
  -h, --help            show this help message and exit
  -t TXT, --txt TXT     set the filename of the input file txt or ttl
  -t0 TTL0, --ttl0 TTL0
                        set the filename of the ttl input 0
  -t1 TTL1, --ttl1 TTL1
                        set the filename of the ttl input 1
  -e EXPORT, --export EXPORT
                        set the export folder
  -v, --verbose         set the verbose mode
  -ft, --flip_table     set to flip the exported table
  -fl, --flip_lattice   set to flip the exported lattice
  -si, --shrink_intent  shrink the intent in the exported lattice
  -se, --shrink_extent  shrink the extent in the exported lattice
  -f GRAPH_FORMAT, --graph_format GRAPH_FORMAT
                        set the format of the exported lattice (pdf, dot,
                        tikz)
  -m, --min_latex       print uri fragments as math ids (in LaTeX)
  -x, --extended_display
                        display full intent/extent in the exported lattice
```

### Run on txt examples

The `linkky.py` script allows to extract link keys from data expressed in a compact text format.

```
$ python3 linkky.py -t txt_data/rca_ontology.txt -e export -v
```

### Run on RDF data

Similarly to txt inputs, the `linkky.py` script supports RDF Data. Here is an example of command. 

```
$ python3 linkky.py -t0 file1.ttl -t1 file1.ttl
```

## Examples

The `txt_data` and `ttl_data` folders contain examples either in the text format or RDF (Turtle). They may be subdivided into subfolders of different types.

Several examples of input files has been provided in the folder `txt_data`.

### Pizza data set

The (in)famous pizza example provided in the initial RCA papers.

### DAM examples

A set of 4 examples provided in a paper under submission (available in both txt and ttl: fca1, fca4, rca-dep, rca2), and some additional similar examples.

### Complexity examples

Examples for testing the theoretical complexity of the process (see corresponing <a href="ttl_data/complex/README.TXT">README</a>).

### Extra examples

Examples illustrating the capability of extracting independent link keys for different classes against the same global class (Relative and Service against Person).

Example illustrating the extraction (or not) of coherent families of link keys when there are no or little anchors.

### MSc experiments

Some experiments on the data set INSEE-GeoNames used in the initial link key candidate paper have been used for testing the program.
The scripts related to these experiments are in the folder `experiments`.
The command `$ python3 experiments/myexperiments.py` allows to perform it.
The process may last for hours.

## Tips

It may be necessary to setup the PYTHONIOENCODING shell variable to UTF-8 for the system to work. This can, and apparently must, be used as:

```
$ PYTHONIOENCODING=UTF-8 python3 linkky.py -t txt_data/dam/rca2.txt -e rca2 -f tikz -m
```

## Formats

### Input

- ttl (preferred): RDF in turtle format (needs two files, one for each data set)
- txt: there are several formats:
  -- fca_database,
  -- fca_concepts,
  -- fca_ontology,
  -- rca_concepts,
  -- rca_ontology, 
  Only the rca_ontology is used nowadays.

### Output

The standard output receives the textual description of the extracted families of compatible link key candidates

In addition, it is possible to generate lattice descriptions in:
- dot: the lattices are output in graphviz/dot format.
- tikz: the dot file is processed and a LaTeX file containing the corresponding TikZ description is generated.
- pdf: the dot file is processed and a PDF file is generated.

and the context description as LaTeX tables.

The `-m` option allows for the generation of a `main.tex` file summarizing the whole

## License

Linkky is made available under the <a href="LICENSE">MIT License</a>.

It is not intended to be used as such for any serious application.

## Credits

Linkky was initially developed by Jérémy Vizzini for the purpose of his master thesis.
This work has been partially supported by the LabEx PERSYVAL-Lab (ANR-11-LABX-0025-01) funded by the French program Investissement d’avenir.
Further development has been partially funded by the ANR Elker project (ANR-17-CE23-0007-01).

It was initially inspired by another <a href="https://github.com/ae-hse/fca">formal concept analysis implementation</a>.

It has been modified by Jérôme Euzenat mostly for improving the display of the output and facilitating input handling.
