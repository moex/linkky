import sys
import argparse
import time
from rdflib import URIRef
from rdflib.graph import Graph
from rdflib.namespace import RDF
from likkylib import *
import datetime


def run_fca_insee_geonames(args, exporter, nb_cities):
    Logger.log('Reading turtles')
    g0, g1 = Graph(), Graph()
    g0.parse('./ttl_data/insee_long.ttl', format="turtle")
    g1.parse('./ttl_data/geonames_long.ttl', format="turtle")

    Logger.log('Reading sameas')
    g2, g3 = Graph(), Graph()
    g2.parse('./ttl_data/sameas_insee_geonames_commu.ttl', format="turtle")
    g3.parse('./ttl_data/sameas_insee_geonames_arrond.ttl', format="turtle")

    sameas_uri = URIRef(u'http://www.w3.org/2002/07/owl#sameAs')
    type_uri = URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#type')
    sd_inssee_uri = URIRef(u'http://rdf.insee.fr/def/geo#subdivisionDe')
    arr_inssee_uri = URIRef(u'http://rdf.insee.fr/def/geo#Arrondissement')
    com_inssee_uri = URIRef('http://rdf.insee.fr/def/geo#Commune')
    fea_geoname_uri = URIRef('http://www.geonames.org/ontology#Feature')

    Logger.log('Loading classes')
    classes_0 = {com_inssee_uri, arr_inssee_uri}
    classes_1 = {fea_geoname_uri}
    ontology = Ontology(classes_0, classes_1)

    Logger.log('Loading instances')
    insee_items = set()
    nb_insee_com, nb_insee_arr = 0, 0
    nb_geoname_feat_com, nb_geoname_feat_ar = 0, 0
    for s, p, o in g0.triples((None, RDF.type, com_inssee_uri)):
        if nb_insee_com < nb_cities:
            ontology.add_instance(o, s)
            insee_items.add(s)
            nb_insee_com += 1
            for s0, p0, o0 in g0.triples((s, sd_inssee_uri, None)):
                insee_items.add(o0)

    for s, p, o in g0.triples((None, RDF.type, arr_inssee_uri)):
        if s in insee_items:
            ontology.add_instance(o, s)
            nb_insee_arr += 1

    for s, p, o in g1.triples((None, RDF.type, None)):
        for p in g2.subjects(sameas_uri, s):
            if p in insee_items:
                ontology.add_instance(o, s)
                nb_geoname_feat_com += 1
                break
        for p in g3.subjects(sameas_uri, s):
            if p in insee_items:
                ontology.add_instance(o, s)
                nb_geoname_feat_ar += 1
                break

    Logger.log('nb_insee_com', nb_insee_com)
    Logger.log('nb_insee_arr', nb_insee_arr)
    Logger.log('nb_geoname_feat_com', nb_geoname_feat_com)
    Logger.log('nb_geoname_feat_ar', nb_geoname_feat_ar)

    inst_ids = set()
    for c_id in it.chain(classes_0, classes_1):
        inst_ids |= ontology.get_instances(c_id)

    Logger.log('Loading properties')
    for (s, p, o) in it.chain(g0, g1):
        if s not in inst_ids or p == type_uri:
            continue
        if p == sameas_uri:
            continue
        if o in inst_ids:
            ontology.add_obj_property(s, p, o)
        elif type(o) is not URIRef:
            ontology.add_val_property(s, p, o)
        else:
            pass

    exporter.substituations = [
        ('rdflib.term.URIRef', ''),
        ('http://www.w3.org/2003/01/geo/wgs84_pos#', 'wgs84_pos:'),
        ('http://rdf.insee.fr/def/geo#', 'insee:'),
        ('http://www.geonames.org/ontology#', 'geonames:')
    ]

    Logger.log('Performing interlinking')
    linker = RCAOntologyLinker(ontology)
    linker.quantifiers = ('∃', '∀∃')

    Logger.log(datetime.datetime.now())
    start_time = time.time()
    max_i, vector_linkkeys = linker.extract_linkkeys(exporter)
    elapsed_time = time.time() - start_time
    Logger.log(datetime.datetime.now())
    Logger.log('Time: %.3f s' % elapsed_time)
    Logger.log('Best: %d' % max_i)
    for i, vector in enumerate(vector_linkkeys):
        print(78 * '-' + '\nVector: %d score: %.2f' % (i, vector[1]))
        for linkkey in vector[0]:
            print(linkkey)


def run():
    if not sys.version_info >= (3, 6):
        print("linkky.py requires python 3.6 or older")

    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--nb_cities', type=int,
                        help='set the number of cities')
    parser.add_argument('-e', '--export', type=str, default=None,
                        help='set the export folder')
    parser.add_argument('-v', '--verbose', action="store_true",
                        help='set the verbose mode')
    parser.add_argument('-ft', '--flip_table', action="store_true",
                        help='set to flip the exported table')
    parser.add_argument('-fl', '--flip_lattice', action="store_true",
                        help='set to flip the exported lattice')
    parser.add_argument('-si', '--shrink_intent', default=True,
                        help='shrink the intent in the exported lattice')
    parser.add_argument('-se', '--shrink_extent', action="store_true",
                        help='shrink the extent in the exported lattice')
    args = parser.parse_args()

    exporter = None
    if args.export is not None:
        exporter = Exporter(args.export)
        exporter.flip_table = args.flip_table
        exporter.flip_lattice = args.flip_lattice
        exporter.shrink_intent = args.shrink_intent
        exporter.shrink_extent = args.shrink_extent
    Logger.set_enable(args.verbose)

    run_fca_insee_geonames(args, exporter, nb_cities=args.nb_cities)


if __name__ == '__main__':
    run()
